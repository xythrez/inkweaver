'''
Package that converts websites into ebooks. Can be invoked by via
commandline as well as programmatically. This file contains wrappers
for the core InkWeaver object.
'''
from .version import __version__
from .cli import InkWeaverCli
from .core import InkWeaver

__all__ = ['__version__', 'InkWeaver', 'InkWeaverCli']
