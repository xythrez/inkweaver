'''Core functionality for InkWeaver API.'''

from threading import Lock
from typing import (Any, Callable, Dict, Iterator, List, Optional, Set, Tuple,
                    Type, Union)

from bs4 import BeautifulSoup as Soup
from ebooklib import epub

from .connection import DownloadManager
from .templates import TemplateRegistry, InkWeaverTemplate
from .utils import DownloadError, TemplateError, EbookError, get_local_img_path
from .version import INKWEAVER_VERSION
from .reference import (CONF_DEFAULTS, IMG_KEEP_ATTRS, HTML_UNWRAPPABLES,
                        HTML_DECOMPOSABLE)


class Volume:
    '''Implementation of an InkWeaver Volume object.

    Use Volume.save() to write to a local epub file. Volume.output_path can be
    overwritten to specify alternative save path.
    '''

    @property
    def series_id(self) -> str:
        '''Short series id string.'''
        return self.template.template_id

    @property
    def series_name(self) -> str:
        '''Full series name as shown in ebook.'''
        return self.template.template_name

    @property
    def volume_id(self) -> str:
        '''Alias to Volume.volume_name.'''
        return self.volume_name

    def __len__(self) -> int:
        return len(self._chapters)

    def __init__(self, idx: int, id_: str, chapters: List[Tuple[str, str]],
                 series: 'Series'):
        self.index: int = idx
        self.volume_name: str = id_
        self._chapters: List[Tuple[str, str]] = chapters
        self.template: InkWeaverTemplate = series.template
        self.output_path: str = series._inkweaver.arguments[
            'output_format'].replace('%SERIES%', series.series_name).replace(
                '%VOLUME%', self.volume_name)
        self._dl_manager: Optional[DownloadManager] = series._dl_manager
        self._image_urls: Set[str] = set()

    def __repr__(self) -> str:
        return (f'<{self.__class__.__module__}.{self.__class__.__qualname__} '
                f'"{self.volume_name}@{self.series_id}" at {hex(id(self))}>')

    def shutdown(self) -> None:
        '''Free resources used by Volume.

        This unlinks the associated DownloadManager and marks the Volume is no
        longer in use.

        Warning: Volume objects are NOT THREAD-SAFE and this function should
        only be called once YOU ARE ABSOLUTELY SURE THE VOLUME IS NOT CURRENTLY
        IN USE!
        '''
        self._dl_manager = None

    def save(self,
             chapter_dl_callback: Optional[Callable[[int], None]] = None,
             chapter_process_callback: Optional[Callable[[int], None]] = None,
             image_dl_callback: Optional[Callable[[int], None]] = None,
             presave_callback: Optional[Callable[[str], None]] = None) -> None:
        '''Save the Volume as a local epub file.

        All errors are wrapped within InkWeaver's wrapper error types.

        Args:
          chapter_dl_callback: Callback function invoked whenever a chapter
            download completes. Must be thread-safe.
          chapter_process_callback: Callback function invoked whenever a
            chapter post-process completes.
          image_dl_callback: Callback function invoked whenever a chapter
            download completes. Must be thread-safe.
          presave_callback: Callback function invoked whenever a epub is about
            to be saved.

        Raises:
          DownloadError: An error occured while downloading resource.
          TemplateError: An error occured while using template.
          EbookError: An error occured while creating ebook.
        '''
        try:
            self._save(chapter_dl_callback, chapter_process_callback,
                       image_dl_callback, presave_callback)
        except (DownloadError, TemplateError, EbookError) as err:
            raise err
        except Exception as err:
            raise EbookError('error occured while writing ebook') from err

    def _save(self, chapter_dl_callback: Optional[Callable[[int], None]],
              chapter_process_callback: Optional[Callable[[int], None]],
              image_dl_callback: Optional[Callable[[int], None]],
              presave_callback: Optional[Callable[[str], None]]) -> None:
        '''Internal function for saving the Volume.

        Args:
          chapter_dl_callback: Callback function invoked whenever a chapter
            download completes. Must be thread-safe.
          chapter_process_callback: Callback function invoked whenever a
            chapter post-process completes.
          image_dl_callback: Callback function invoked whenever a chapter
            download completes. Must be thread-safe.
          presave_callback: Callback function invoked whenever a epub is about
            to be saved.

        Raises:
          DownloadError: An error occured while downloading resource.
          TemplateError: An error occured while using template.
          EbookError: An error occured while creating ebook.
        '''
        if self._dl_manager is None:
            raise EbookError('method invoked on invalidated volume')

        try:
            chapter_urls = list(map(lambda x: x[1], self._chapters))
        except IndexError as err:
            raise TemplateError('invalid volume info format: ' +
                                self.series_id) from err

        chapters = self._dl_manager.download_all_resources(
            chapter_urls, chapter_dl_callback)

        # Cast to list to force eager evaluation on zip().
        chapter_infos = list(
            map(
                lambda x: self._process_chapters(x, chapter_process_callback),
                zip(map(lambda x: x[0], self._chapters),
                    map(lambda x: x.data, chapters))))

        images = self._dl_manager.download_all_resources(
            list(self._image_urls), image_dl_callback)

        # Cast to list to force eager evaluation on zip().
        image_infos = list(
            zip(map(get_local_img_path, self._image_urls),
                map(lambda x: x.data, images)))

        if presave_callback is not None:
            presave_callback(self.output_path)
        book = self._gen_epub(chapter_infos, image_infos)
        epub.write_epub(self.output_path, book)

    def _gen_epub(self, chapter_infos: List[Tuple[str, str]],
                  image_infos: List[Tuple[str, bytes]]) -> epub.EpubBook:
        '''Generate a EpubBook object for Volume.

        Args:
          chapter_infos: list of tuples containing chapter title and body.
          image_infos: list of tuples containing local image paths and data.

        Returns:
          The generated EpubBook object.
        '''
        book = epub.EpubBook()
        book.set_title(f'{self.series_name} - {self.volume_name}')
        book.set_language(self.template.lang)
        book.add_metadata(None, 'meta', '', {
            'name': 'calibre:series',
            'content': self.series_name
        })
        book.add_metadata(None, 'meta', '', {
            'name': 'calibre:series_index',
            'content': f'{self.index + 1}.0'
        })
        for author in self.template.authors:
            book.add_author(author)

        book.add_item(epub.EpubNcx())
        book.add_item(epub.EpubNav())
        chapter_list = ['nav']
        stylesheet = epub.EpubItem(uid='css',
                                   file_name="style/stylesheet.css",
                                   media_type="text/css",
                                   content=self.template.stylesheet)
        book.add_item(stylesheet)
        book.get_item_with_id('nav').add_item(stylesheet)

        for i, chapter_info in enumerate(chapter_infos):
            chapter_title, chapter_html = chapter_info
            chapter = epub.EpubHtml(title=chapter_title,
                                    file_name=f'chap_{i}.xhtml',
                                    lang=self.template.lang,
                                    content=chapter_html)
            chapter.add_item(stylesheet)
            chapter_list.append(chapter)
            book.add_item(chapter)

        for image_path, image_data in image_infos:
            image = epub.EpubItem(file_name=image_path, content=image_data)
            book.add_item(image)
        book.toc = chapter_list
        book.spine = chapter_list
        return book

    def _process_chapters(
        self, chapter_info: Tuple[str, bytes],
        chapter_process_callback: Optional[Callable[[int], None]]
    ) -> Tuple[str, str]:
        '''Process the chapter page using template functions.

        Args:
          chapter_info: tuple containing chapter title and raw html body.
          chapter_process_callback: Callback function invoked whenever a
            chapter post-process completes.

        Returns:
          A tuple consisting of chapter title and processed html body.
        '''
        name, html = chapter_info
        try:
            html_str = self.template.extract_chapter(
                name, html.decode(self.template.encoding))
        except Exception as err:
            raise TemplateError('error while extracting content: ' +
                                self.series_id) from err
        soup = Soup(html_str, self.template.parser)
        for img in soup.find_all('img', src=True):
            img.attrs = {
                key: value
                for key, value in img.attrs.items() if key in IMG_KEEP_ATTRS
            }
            self._image_urls.add(img['src'])
            img['src'] = get_local_img_path(img['src'])
        for unwrappable in soup.find_all(HTML_UNWRAPPABLES):
            unwrappable.unwrap()
        for decomposable in soup.find_all(HTML_DECOMPOSABLE):
            decomposable.decompose()
        if chapter_process_callback is not None:
            chapter_process_callback(len(self._chapters))
        return (name, str(soup))


class Series:
    '''Implementation of an InkWeaver Series object.

    Volumes in the series are lazily initialized. The Series
    object can be iterated/indexed as a standard Python dictionary.
    '''

    @property
    def series_id(self) -> str:
        '''Short series id string.'''
        return self.template.template_id

    @property
    def series_name(self) -> str:
        '''Full series name as shown in ebook.'''
        return self.template.template_name

    @property
    def volumes(self) -> List[Volume]:
        '''Volumes available in Series.'''
        if not self._volume_cache:
            self._init_volumes()
        return list(self._volume_cache.values())

    @property
    def initialized(self) -> bool:
        '''Whether volumes have been initalized'''
        return bool(self._volume_cache)

    def __init__(self, inkweaver: 'InkWeaver',
                 template: Type[InkWeaverTemplate]):
        self._init_lock: Lock = Lock()
        self.template: InkWeaverTemplate = template()
        self._inkweaver: InkWeaver = inkweaver
        self._volume_cache: Dict[str, Volume] = {}
        self._dl_manager: Optional[DownloadManager] = None

    def _init_volumes(self) -> None:
        '''Download ToC and initalize Volume objects.'''
        with self._init_lock:
            if self._volume_cache:
                return
            new_volume_cache = {}
            if self._dl_manager is None:
                self._dl_manager = DownloadManager(self.template.downloader,
                                                   self._inkweaver.arguments)
            res = self._dl_manager.download_resource(self.template.toc_url)
            try:
                toc = self.template.extract_volumes(
                    res.data.decode(self.template.encoding))
            except Exception as err:
                raise TemplateError('error while extracting volumes: ' +
                                    self.series_id) from err
            for i, volume_info in enumerate(toc):
                name, chapters = volume_info
                new_volume_cache[name] = Volume(i, name, chapters, self)
            self._volume_cache = new_volume_cache

    def __contains__(self, id_: str) -> bool:
        if not self._volume_cache:
            self._init_volumes()
        return id_ in self._volume_cache

    def __getitem__(self, id_: str) -> Volume:
        if not self._volume_cache:
            self._init_volumes()
        volume = self._volume_cache.get(id_)
        if volume is not None:
            return volume
        raise IndexError(f'{id_} is not a valid volume name')

    def __iter__(self) -> Iterator[Volume]:
        yield from self.volumes

    def __repr__(self) -> str:
        return (f'<{self.__class__.__module__}.{self.__class__.__qualname__} '
                f'"{self.series_id}" at {hex(id(self))}>')

    def invalidate_caches(self, close_connections: bool = False) -> None:
        '''Invalidate all cached volumes and resources in series.

        Args:
          close_connections: if True, will also close the DownloadManager
        '''
        with self._init_lock:
            for volume in self._volume_cache.values():
                volume.shutdown()
            self._volume_cache = {}
            if close_connections and self._dl_manager is not None:
                self._dl_manager.close()
                self._dl_manager = None
            elif self._dl_manager is not None:
                self._dl_manager.clear_cache()

    def shutdown(self) -> None:
        '''Free resources used by Series and performs cleanup.

        Alias to Series.invalidate_caches(True).
        '''
        self.invalidate_caches(True)


class InkWeaver:
    '''InkWeaver core implementaton.

    Series and Volumes can be obtained via indexing into the object as a
    standard python dictionary.
    '''

    @property
    def version(self) -> Tuple[int, Union[int, str]]:
        '''InkWeaver major, minor version tuple.

        Development versions show '%DEV_VERSION% instead of the minor version.'
        '''
        return INKWEAVER_VERSION

    @property
    def series(self) -> List[Series]:
        '''Series available in InkWeaver.'''
        return list(self._series_cache.values())

    def __init__(self) -> None:
        self.arguments: Dict[str, Any] = CONF_DEFAULTS.copy()
        self._series_cache: Dict[str, Series] = {
            v.series_id: v
            for v in map(lambda x: Series(self, x), TemplateRegistry())
        }

    def set_argument(self, arg_k: str, arg_v: Any) -> None:
        '''Set an argument.

        Default values can be found in inkweaver.reference.CONF_DEFAULTS.

        Args:
          arg_k: Argument key.
          arg_v: Argument value.

        Raises:
          NameError: If argument key is not valid.
          TypeError: If argument value is not of the right type.
        '''
        if arg_k not in self.arguments:
            raise NameError(f'{arg_k} is not a valid argument!')
        argtype = type(CONF_DEFAULTS[arg_k])
        if not isinstance(arg_v, argtype):
            raise TypeError(f'{arg_k} is not of type {argtype}!')
        if arg_k == 'interval' and arg_v < 0:
            arg_v = 0
        elif arg_k == 'max_retries' and arg_v < 1:
            arg_v = 1
        elif arg_k == 'threads' and arg_v < 1:
            arg_v = 1
        elif arg_k == 'timeout' and arg_v < 1:
            arg_v = 1
        self.arguments[arg_k] = arg_v

    def __contains__(self, id_: str) -> bool:
        return id_ in self._series_cache

    def __getitem__(self, id_: str) -> Series:
        series = self._series_cache.get(id_)
        if series is not None:
            return series
        raise IndexError(f'{id_} is not a valid series id')

    def __iter__(self) -> Iterator[Series]:
        yield from self.series
