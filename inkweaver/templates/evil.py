'''
Module containing the "A Practical Guide to Evil" template
'''
import re
from bs4 import BeautifulSoup as Soup
from . import InkWeaverTemplate


class EvilTemplate(InkWeaverTemplate):
    '''Template for "A Practical Guide to Evil"'''
    toc_url = 'https://practicalguidetoevil.wordpress.com/table-of-contents/'
    template_id = 'evil'
    template_name = 'A Practical Guide to Evil'
    authors = ['erraticerrata']

    def extract_volumes(self, html_body):
        volume_info = []
        for tag in Soup(html_body, self.parser).find('div', {
                'class': 'entry-content'
        }).find_all(['h2', 'ul'], recursive=False):
            if tag.name == 'h2':
                volume_info.append((tag.text.strip(), []))
            else:
                volume_info[-1][1].extend([
                    (x.text.strip(), x['href'])
                    for x in tag.find_all('a', href=True)
                ])
        return volume_info

    def extract_chapter(self, title, html_body):
        soup = Soup(html_body, self.parser)
        content = soup.find('div', {'class': 'entry-content'})
        for tag in content.find_all(id='jp-post-flair'):
            tag.decompose()
        for tag in content.find_all(
                'div', {'id': re.compile('atatags-[0-9]+-[0-9]+')}):
            tag.decompose()
        for tag in content.find_all('div', {'class': 'wpcnt'}):
            tag.decompose()
        title_tag = soup.new_tag('h1')
        title_tag.string = title
        content.insert(0, title_tag)
        return '\n'.join([str(x) for x in content.children])
