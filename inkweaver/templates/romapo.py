'''
Module containing the "Romantically Apocalyptic" template
'''
from .royalroad import RoyalRoadTemplate


class MetaworldTemplate(RoyalRoadTemplate):
    '''Template for "Romantically Apocalyptic"'''
    toc_url = 'https://www.royalroad.com/fiction/56990/romantically-apocalyptic-webcomic'
    template_id = 'romapo'
    template_name = 'Romantically Apocalyptic'
    authors = ['Vitaly S Alexius']
