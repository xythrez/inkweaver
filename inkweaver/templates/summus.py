'''
Module containing the "Summus Proelium" template
'''
from .royalroad import RoyalRoadTemplate


class SummusTemplate(RoyalRoadTemplate):
    '''Template for "Summus Proelium"'''
    toc_url = 'https://www.royalroad.com/fiction/58157/summus-proelium'
    template_id = 'summus'
    template_name = 'Summus Proelium'
    authors = ['Cerulean Scrawling']
