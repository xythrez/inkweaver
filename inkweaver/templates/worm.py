'''
Module containing the "Worm" template
'''
from bs4 import BeautifulSoup as Soup
from . import InkWeaverTemplate


class WormTemplate(InkWeaverTemplate):
    '''Template for "Worm"'''
    toc_url = 'https://parahumans.wordpress.com/table-of-contents/'
    template_id = 'worm'
    template_name = 'Worm'
    authors = ['Wildbow']

    def extract_volumes(self, html_body):
        e2_link = ('E.2',
                   'https://parahumans.wordpress.com/2013/11/05/teneral-e-2/')
        volume_info = []
        content = Soup(html_body, self.parser).find('div',
                                                    {'class': 'entry-content'})
        for tag in content.find_all(id='jp-post-flair'):
            tag.decompose()
        toc_text = [
            text.strip() for text in content.text.split('\n') if text.strip()
        ]
        links = {
            x.text.strip(): x['href']
            for x in content.find_all('a', href=True) if x.text.strip()
        }
        for title in toc_text:
            if title not in links and title != e2_link[0]:
                volume_info.append((title, []))
            elif title != e2_link[0]:
                volume_info[-1][1].append((title, links[title]))
            else:
                volume_info[-1][1].append(e2_link)
        return volume_info

    def extract_chapter(self, title, html_body):
        soup = Soup(html_body, self.parser)
        content = soup.find('div', {'class': 'entry-content'})
        title_tag = soup.new_tag('h1')
        title_tag.string = title
        content.insert(0, title_tag)
        for tag in [
                x for x in content.find_all('a', href=True)
                if x.text.strip() in ('Last Chapter', 'Next Chapter')
        ]:
            tag.decompose()
        for tag in content.find_all(id='jp-post-flair'):
            tag.decompose()
        for tag in [
                x for x in content.children if not x.text and x.name != 'br'
        ]:
            tag.decompose()
        return '\n'.join([str(x) for x in content.children])
