'''
Module containing the "Inexorable Chaos" template
'''
from .royalroad import RoyalRoadTemplate


class ChaosTemplate(RoyalRoadTemplate):
    '''Template for "Inexorable Chaos"'''
    toc_url = 'https://www.royalroad.com/fiction/22546/inexorable-chaos'
    template_id = 'chaos'
    template_name = 'Inexorable Chaos'
    authors = ['Quasieludo']
