'''
Module containing the "Metaworld Chronicles" template
'''
from .royalroad import RoyalRoadTemplate


class MetaworldTemplate(RoyalRoadTemplate):
    '''Template for "Metaworld Chronicles"'''
    toc_url = 'https://www.royalroad.com/fiction/14167/metaworld-chronicles'
    template_id = 'metaworld'
    template_name = 'Metaworld Chronicles'
    authors = ['Wutosama']
