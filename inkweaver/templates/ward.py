'''
Module containing the "Ward" template
'''
import re
from bs4 import BeautifulSoup as Soup
from . import InkWeaverTemplate


class WardTemplate(InkWeaverTemplate):
    '''Template for "Ward"'''
    toc_url = 'https://www.parahumans.net/table-of-contents/'
    template_id = 'ward'
    template_name = 'Ward'
    authors = ['Wildbow']

    def extract_volumes(self, html_body):
        url_base = 'https://www.parahumans.net'
        volume_info = []
        for li_soup in Soup(html_body, self.parser).find_all(
                'ul', id=re.compile('^menu-table-of-contents.*')):
            for tags in li_soup.find_all('li', recursive=False):
                volume_info.append(
                    (tags.find('a', recursive=False).text.strip(),
                     [(x.text.strip(), url_base + x['href'])
                      for x in tags.find('ul').find_all('a', href=True)]))
        return volume_info

    def extract_chapter(self, title, html_body):
        soup = Soup(html_body, self.parser)
        title_tag = soup.new_tag('h1')
        title_tag.string = title
        content = soup.find('div', {'class': 'entry-content'})
        for tag in [
                x for x in content.find_all('a', href=True)
                if x.text.strip() in ('Previous Chapter', 'Next Chapter',
                                      'Last Chapter')
        ]:
            tag.decompose()
        for tag in content.find_all(
                'div', {
                    'class': [
                        'sharedaddy sd-sharing-enabled',
                        ('sharedaddy sd-block sd-like '
                         'jetpack-likes-widget-wrapper '
                         'jetpack-likes-widget-unloaded')
                    ]
                }):
            tag.decompose()
        for tag in [
                x for x in content.children if not x.text and x.name != 'br'
        ]:
            tag.decompose()
        content.insert(0, title_tag)
        return '\n'.join([str(x) for x in content.children])
