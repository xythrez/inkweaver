'''
Module containing the "RoyalRoad" meta-template
'''
import re
from functools import reduce
from bs4 import BeautifulSoup as Soup
from . import InkWeaverTemplate


class RoyalRoadTemplate(InkWeaverTemplate, abstract=True):
    '''Meta-template for "RoyalRoad"'''
    rr_embed_title = True
    disp_none_re = re.compile(r'^\s*\.([0-9a-zA-Z]+)\s*{'
                              r'\s*display:\s*none;'
                              r'\s*speak:\s*never;'
                              r'\s*}\s*$')

    def extract_volumes(self, html_body):
        url_base = 'https://www.royalroad.com'
        chapters = [(x.get_text().strip(), url_base + x['href'])
                    for x in Soup(html_body, self.parser).find(
                        id='chapters').find_all('a', href=True)
                    if 'class' not in x.parent.attrs]
        return [('Complete', chapters)]

    def extract_chapter(self, title, html_body):
        soup = Soup(html_body, self.parser)
        content = soup.find('div', {'class': 'chapter-inner chapter-content'})
        if self.rr_embed_title:
            title_tag = soup.new_tag('h1')
            title_tag.string = title
            content.insert(0, title_tag)
        match = reduce(
            lambda x, y: x
            if x is not None else self.disp_none_re.match(y.text),
            soup.find_all('style'), None)
        if match:
            for tag in soup.find_all('p', {'class': match[1]}):
                tag.decompose()
        return '\n'.join([str(x) for x in content.children])
