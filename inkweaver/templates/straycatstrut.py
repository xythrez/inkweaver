'''
Module containing the "Stray Cat Strut" template
'''
import re
from .royalroad import RoyalRoadTemplate


class StrayCatStrutTemplate(RoyalRoadTemplate):
    '''Template for "Stray Cat Strut"'''
    toc_url = ('https://www.royalroad.com/fiction/'
               '33600/stray-cat-strut-a-young-ladys-journey-to-becoming')
    template_id = 'straycatstrut'
    template_name = 'Stray Cat Strut'
    authors = ['Ravens Dagger']

    rr_embed_title = False

    def extract_volumes(self, html_body):
        chapters = super().extract_volumes(html_body)[0][1]
        volume_re = re.compile('^Chapter One')
        skip_re = re.compile('^Stray Cat Strut')
        volume_info = []
        for title, url in chapters:
            if volume_re.match(title):
                volume_info.append(
                    (f'Volume {len(volume_info) + 1}', [(title, url)]))
            elif not skip_re.match(title):
                volume_info[-1][1].append((title, url))
        return volume_info
