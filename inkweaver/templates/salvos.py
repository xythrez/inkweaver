'''
Module containing the "Salvos" template
'''
import re
from .royalroad import RoyalRoadTemplate


class SalvosTemplate(RoyalRoadTemplate):
    '''Template for "Salvos"'''
    toc_url = 'https://www.royalroad.com/fiction/37438/salvos'
    template_id = 'salvos'
    template_name = 'Salvos'
    authors = ['MelasDelta']

    def extract_volumes(self, html_body):
        return [(x[0], [y for y in x[1] if re.match(r'^[0-9]+\.', y[0])])
                for x in super().extract_volumes(html_body)]
