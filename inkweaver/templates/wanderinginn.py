'''
Module containing "The Wandering Inn" template
'''
import re
from bs4 import BeautifulSoup as Soup
from . import InkWeaverTemplate


class WanderingInnTemplate(InkWeaverTemplate):
    '''Template for "The Wandering Inn"'''
    toc_url = 'https://wanderinginn.com/table-of-contents/'
    template_id = 'wanderinginn'
    template_name = 'The Wandering Inn'
    authors = ['pirateaba']

    def extract_volumes(self, html_body):
        vol_tags = Soup(html_body,
                        self.parser).find(id='table-of-contents').find_all(
                            attrs={'class': ['volume-wrapper']})
        return [(x.find(attrs={
            'class': ['volume-header']
        }).text.strip(), [
            (z.text.strip(), z['href']) for z in
            [y.find('a') for y in x.find_all(attrs={'class': ['body-web']})]
        ]) for x in vol_tags]

    def extract_chapter(self, title, html_body):
        soup = Soup(html_body, self.parser)
        content = soup.find('div', {'class': 'entry-content'})
        for wrapper in content.find_all('div', id='paste-embed-wrapper'):
            wrapper.unwrap()
        announcement_tag = content.find('p').find('strong')
        if announcement_tag and announcement_tag.text[0] == '(':
            announcement_tag.decompose()
        note_tag = content.find('strong',
                                string=re.compile(r'^Author.s Notes?:'))
        if not note_tag:
            note_tag = content.find('strong',
                                    string=re.compile(r'^After Chapter '
                                                      r'Thoughts?:'))
        if note_tag:
            for tag in note_tag.parent.find_all_next('p'):
                tag.decompose()
            note_tag.parent.decompose()
        title_tag = soup.new_tag('h1')
        title_tag.string = title
        return '\n'.join([str(title_tag)] + [
            str(p) for p in content.find_all('p', recursive=False)
            if p.previous_element.previous_element.name != 'hr'
        ])
