'''
Module containing the "Defiance of the Fall" template
'''
from .royalroad import RoyalRoadTemplate


class DefianceTemplate(RoyalRoadTemplate):
    '''Template for "Defiance of the Fall"'''
    toc_url = 'https://www.royalroad.com/fiction/24709/defiance-of-the-fall'
    template_id = 'defiance'
    template_name = 'Defiance of the Fall'
    authors = ['TheFirstDefier']
