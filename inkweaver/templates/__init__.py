'''Series templates for InkWeaver.'''

import abc
import importlib
import pkgutil
from functools import reduce
from typing import Iterator, List, Optional, Set, Type, Tuple, Union

from ..downloaders import Downloader
from ..downloaders.curl import CurlDownloader
from ..reference import DEFAULT_CSS
from ..utils import Singleton


class InkWeaverTemplate(abc.ABC):
    '''Abstract InkWeaver template class.'''

    def __init_subclass__(cls: Type, abstract: bool = False, **kwargs) -> None:
        super().__init_subclass__(**kwargs)
        registry = TemplateRegistry()
        if not abstract and cls not in registry:
            registry.register_template(cls, lazy=True)

    # List of authors.
    authors: List[str] = ['unknown']
    # Ebook language metadata.
    lang: str = 'en'
    # CSS stylesheet string.
    stylesheet: str = DEFAULT_CSS
    # Book and website encoding information.
    encoding: str = 'UTF-8'
    # Downloader to use.
    downloader: Type[Downloader] = CurlDownloader
    # BeautifulSoup4 parser to use.
    parser: str = 'lxml'

    @property
    @abc.abstractmethod
    def template_id(self) -> str:
        '''Short template id string.'''

    @property
    @abc.abstractmethod
    def template_name(self) -> str:
        '''Full template name as shown in ebook.'''

    @property
    @abc.abstractmethod
    def toc_url(self) -> str:
        '''URL for table-of-contents page.'''

    @abc.abstractmethod
    def extract_volumes(
            self, html_body: str) -> List[Tuple[str, List[Tuple[str, str]]]]:
        '''Extract volumes information from table-of-contents page.

        Args:
          html_body: The HTML content of the toc.

        Returns:
          List of volume tuples, each containing the name and a list of title,
          chapter URL tuples.
        '''

    @abc.abstractmethod
    def extract_chapter(self, title, html_body: str) -> str:
        '''Extract chapter content from page

        Args:
          title: The title of the chapter.
          html_body: The HTML content of the page.

        Returns:
          HTML content of the chapter to be included in ebook.
        '''


class TemplateRegistry(metaclass=Singleton):
    '''Template registry class.

    A single template registry is used across each python interpreter, with
    each InkWeaver object sharing the registry. Added templates will be visible
    across all new InkWeaver object instances.
    '''

    def __init__(self) -> None:
        self._registry: Set[Type[InkWeaverTemplate]] = set()

    def __contains__(self, obj: Union[str, Type[InkWeaverTemplate]]) -> bool:
        if isinstance(obj, str):
            return self.get_template_by_id(obj) is not None
        return obj in self._registry

    def __iter__(self) -> Iterator[Type[InkWeaverTemplate]]:
        yield from self.get_all_templates()

    def __len__(self) -> int:
        return len(self._registry)

    def __getitem__(self, id_: str) -> Type[InkWeaverTemplate]:
        template = self.get_template_by_id(id_)
        if template is None:
            raise IndexError(f'{id_} is not a valid template id')
        return template

    def _init_templates(self) -> None:
        '''Detect and load templates in "InkWeaver.templates".'''
        if not self._registry:
            for _, name, _ in pkgutil.walk_packages(__path__):
                importlib.import_module(f'{__name__}.{name}')

    def register_template(self,
                          template: Type[InkWeaverTemplate],
                          lazy: bool = False) -> None:
        '''Register template class.

        Args:
          template: template class to register.
          lazy: if True _init_templates() is called automatically.
        '''
        if not lazy:
            self._init_templates()
        self._registry.add(template)

    def get_template_by_id(self,
                           id_: str) -> Optional[Type[InkWeaverTemplate]]:
        '''Get template class by id.

        Args:
          id_: template class id.

        Returns:
          Template class with specified id, None if not available.
        '''
        self._init_templates()
        return reduce(lambda x, y: y
                      if y.template_id == id_ else x, self._registry, None)

    def get_all_templates(self) -> List[Type[InkWeaverTemplate]]:
        '''Get all template classes.

        Returns:
          New list of all template classes.
        '''
        self._init_templates()
        return list(self._registry)


__all__ = ['TemplateRegistry', 'InkWeaverTemplate']
