'''
Module containing the "Pale" template
'''
from bs4 import NavigableString, BeautifulSoup as Soup
from . import InkWeaverTemplate


class WormTemplate(InkWeaverTemplate):
    '''Template for "Pale"'''
    toc_url = 'https://palewebserial.wordpress.com/table-of-contents/'
    template_id = 'pale'
    template_name = 'Pale'
    authors = ['Wildbow']

    def extract_volumes(self, html_body):
        content = Soup(html_body, self.parser).find('div',
                                                    {'class': 'entry-content'})
        volume_info = list(
            zip([
                x.text.strip()
                for x in content.find_all('p', recursive=False, style=False)
            ], [[(y.text.strip(), y['href'])
                 for y in x.find_all('a', href=True)]
                for x in content.find_all('p', recursive=False, style=True)]))
        return volume_info

    def extract_chapter(self, title, html_body):
        soup = Soup(html_body, self.parser)
        content = soup.find('div', {'class': 'entry-content'})
        content.find().string = soup.find('h1', {
            'class': 'entry-title'
        }).text + ': ' + content.find().text
        for tag in content.find_all('a', href=True):
            tag.decompose()
        for tag in content.find_all(id='jp-post-flair'):
            tag.decompose()
        for tag in [
                x for x in content.children
                if not isinstance(x, NavigableString) and not x.text.strip()
                and x.name != 'br'
        ]:
            tag.decompose()
        return '\n'.join([str(x) for x in content.children])
