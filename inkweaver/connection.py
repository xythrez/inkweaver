'''InkWeaver remote connection components.'''

from concurrent.futures import ThreadPoolExecutor
from queue import Queue
from threading import Lock
from typing import Any, Callable, Dict, List, Optional, Type

from .downloaders import Downloader, Resource
from .utils import DownloadError


class DownloadManager:
    '''Parallel download manager class

    A separate manager is used for each Series due to potential incompatable
    downloaders.
    '''

    @property
    def max_connections(self) -> int:
        '''Maximum number of outbound connections.'''
        return len(self._connections)

    def __init__(self, downloader: Type[Downloader], settings: Dict[str, Any]):
        max_connections = settings['threads'] if 'threads' in settings else 1
        self._cache: Dict[str, Resource] = {}
        self._downloader: Type[Downloader] = downloader
        self._executor: Optional[ThreadPoolExecutor] = None
        self._executor_lock: Lock = Lock()
        self._settings: Dict[str, Any] = settings
        self._pool: Queue[Downloader] = Queue(max_connections)
        self._connections: List[Downloader] = []
        for _ in range(0, max_connections):
            con = downloader(settings)
            self._connections.append(con)
            self._pool.put(con)

    def _alloc_connection(self) -> Downloader:
        '''Allocate a downloader connection.

        If no downloaders are available, DownloadManager will wait until a
        downloader connection is available. Calling this function after the
        manager is closed may result in undefined behavior.

        Returns:
          Downloader available for use.
        '''
        return self._pool.get()

    def _free_connection(self, con: Downloader) -> bool:
        '''Free a downloader connection.

        Args:
          con: The connection to free.

        Returns:
          True if the connection is put back into the available pool. False
          if otherwise.
        '''
        if con in self._connections:
            self._pool.put(con)
            return True
        return False

    def close(self) -> None:
        '''Close the DownloadManager and its associated connections.

        If a ThreadPoolExecutor is allocated for the manager, the executor will
        be shutdown as well.

        Calling close with ongoing downloads may result in undefined behavior
        '''
        with self._executor_lock:
            if self._executor is not None:
                self._executor.shutdown()
        self._pool.empty()
        for con in self._connections:
            con.close()

    def __enter__(self):
        return self

    def __exit__(self, exit_type, value, traceback):
        self.close()
        return False

    def download_resource(self, url: str) -> Resource:
        '''Download a single resource from a URL.

        Args:
          url: URL of the resource.

        Returns:
          Resource object associated with the URL.

        Raises:
          DownloadError: An error occured while downloading resource.
        '''
        return self._download(url, None)

    def download_all_resources(
            self,
            urls: List[str],
            callback: Optional[Callable[[int],
                                        None]] = None) -> List[Resource]:
        '''Parallel download all resources from a list of URLs.

        Args:
          urls: List of URLs.
          callback: A callback function to invoke each time a resource is
            successfully downloaded. Callback function must be thread-safe
            as there is no guarantee which download thread it is invoked on.

        Returns:
          A list of Resource objects mapped against the URL list.

        Raises:
          DownloadError: An error occured while downloading a resource.
        '''
        with self._executor_lock:
            if self._executor is None:
                self._executor = ThreadPoolExecutor(
                    max_workers=self.max_connections)

        fcb = (lambda: callback(len(urls))) if callback is not None else None
        results = self._executor.map(lambda x: self._download(x, fcb), urls)
        return list(results)

    def _download(self, url: str,
                  callback: Optional[Callable[[], None]]) -> Resource:
        '''Downloader thread function

        Args:
          url: URL of the resource
          callback: callback function to invoke after download completes.

        Returns:
          Resource object associated with the URL.

        Raises:
          DownloadError: An error occured while downloading a resource.
        '''
        # The cache itself is NOT THREAD-SAFE. However data races do not affect
        # program outcome
        if url in self._cache:
            data = self._cache[url]
        else:
            connection = self._alloc_connection()
            try:
                data = connection.get_resource(url)
                self._cache[url] = data
            except Exception as err:
                raise DownloadError('Failed to download resource') from err
            finally:
                self._free_connection(connection)

        if callback is not None:
            callback()
        return data

    def clear_cache(self) -> None:
        '''Clear the internal download cache.'''
        self._cache = {}
