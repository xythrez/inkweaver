'''libcurl related downloaders.'''

import time
from io import BytesIO

import pycurl

from . import Downloader


class CurlDownloader(Downloader):
    '''Downloader implemented with pycurl.'''

    def __init__(self, settings):
        super().__init__(settings)
        self.last_ts: float = 0
        self.curl: pycurl.Curl = pycurl.Curl()
        self.curl_retry_interval: int = settings['interval']
        self.curl_max_retries: int = settings['max_retries']
        self.curl.setopt(pycurl.TIMEOUT, settings['timeout'])
        self.curl.setopt(pycurl.USERAGENT, settings['user_agent'])
        self.curl.setopt(pycurl.FOLLOWLOCATION, True)
        # Workaround for some servers not supporting HTTP/2
        self.curl.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_1_1)

    def on_close(self):
        self.curl.close()

    def on_fetch(self, url):
        self.curl.setopt(pycurl.URL, url.encode('UTF-8'))
        diff = self.last_ts + self.curl_retry_interval - time.monotonic()
        if diff > 0:
            time.sleep(diff)
        last_err = None
        for _ in range(0, self.curl_max_retries):
            try:
                with BytesIO() as buf:
                    self.curl.setopt(pycurl.WRITEDATA, buf)
                    self.curl.perform()
                    data = buf.getvalue()
                    if data:
                        return data
                    raise IOError('URL contains empty resource')
            except (pycurl.error, TypeError, UnicodeError, IOError) as err:
                last_err = err
            finally:
                self.last_ts = time.monotonic()
        raise last_err
