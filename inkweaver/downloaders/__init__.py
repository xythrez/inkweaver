'''Downloaders for InkWeaver.'''

import abc
from threading import Lock
from typing import Any, Dict

from ..utils import sha256sum


class Resource:
    '''Resource wrapper class with built-in SHA256 checksum.

    Resource data is stored as a bytes object in Resource.data.
    '''

    @property
    def checksum(self) -> str:
        '''SHA256 checksum for resource.'''
        return sha256sum(self.data)

    def __init__(self, data: bytes):
        self.data: bytes = data

    def __repr__(self):
        return f'<resource \'0x{self.checksum}\'>'


class Downloader(abc.ABC):
    '''Abstract implementation of a Downloader.

    Downloaders are used by Templates to download remote resources. Flags are
    implemented independently by each downloader class. The specific type of
    downloader used is specified within the template.

    Downloaders are automatically thread-safe. There is no need to implement
    additional locking on exposed abstract methods.
    '''

    @abc.abstractmethod
    def __init__(self, settings: Dict[str, Any]):
        self._lock: Lock = Lock()
        self._closed: bool = False

    @abc.abstractmethod
    def on_fetch(self, url: str) -> bytes:
        '''Remote resource fetch logic.

        Returns:
          Remote resource bytestring.
        '''

    @abc.abstractmethod
    def on_close(self) -> None:
        '''Downloader cleanup logic.

        Called when the downloader is no longer used.'''

    def get_resource(self, url: str) -> Resource:
        '''Get a remote resource using the downloader.

        Args:
          url: UTF-8 encoded URL.

        Returns:
          Remote resource data.
        '''
        with self._lock:
            return Resource(self.on_fetch(url))

    def close(self) -> None:
        '''Shutdown downloader to free resources and prevent further use.'''
        with self._lock:
            self._closed = True
            return self.on_close()

    def __enter__(self):
        return self

    def __exit__(self, exit_type, value, traceback):
        self.close()
        return False
