#!/usr/bin/env python3
'''Entry point for inkweaver package.

When compiled into a binary, this file is moved outside of the package into the
base directory.
'''
import os
import sys
from importlib.util import find_spec


def sanity() -> None:
    '''Execute sanity checks prior to launching inkweaver.main().'''
    # Check Python version
    if sys.version_info <= (3, 6, 0):
        sys.exit('ERROR: Minimum Python version 3.6.0 required')

    # Check native dependencies exist
    for depname in ('lxml', 'pycurl'):
        if not find_spec(depname):
            sys.exit(f'ERROR: Missing dependency detected: {depname}')

    # Check for direct call of __main__.py
    if __package__ is None and not hasattr(sys, 'frozen'):
        path = os.path.realpath(os.path.abspath(__file__))
        sys.path.insert(0, os.path.dirname(os.path.dirname(path)))


if __name__ == '__main__':
    sanity()
    from inkweaver import InkWeaverCli
    app = InkWeaverCli(sys.argv)
    app.run()
