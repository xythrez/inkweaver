'''InkWeaver internal reference constants.'''

from typing import Any, Dict, List

# Default arguments for InkWeaverCli
CLI_DEFAULTS: Dict[str, Any] = {
    'noninteractive': False,
    'verbose': False,
    'list': False,
    'series': None,
    'volumes': []
}
# Default arguments for InkWeaver
CONF_DEFAULTS: Dict[str, Any] = {
    'timeout':
    300,
    'interval':
    1,
    'max_retries':
    3,
    'threads':
    1,
    'user_agent': ('Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:122.0) '
                   'Gecko/20100101 Firefox/122.0'),
    'output_format':
    '%SERIES% - %VOLUME%.epub',
}
# HTML image attributes to keep
IMG_KEEP_ATTRS: List[str] = ['class', 'id', 'src', 'height', 'width', 'style']
# HTML tags to unwrap.
HTML_UNWRAPPABLES: List[str] = ['a']
# HTML tags to decompose.
HTML_DECOMPOSABLE: List[str] = ['script', 'iframe', 'embed']

# CSS credit: https://notjohnkdp.blogspot.com/2013/07/heres-my-style-sheet.html
DEFAULT_CSS: str = '''
@namespace epub "http://www.idpf.org/2007/ops";

ol {
    list-style-type:none
}

p {
    margin-top:0.0em;
    margin-bottom:0.0em;
    text-indent:1.5em;
    text-align:justify;
}

p.first {
    margin-top:0.5em;
    margin-bottom: 0.0em;
    text-indent:0.0em;
    text-align:justify;
}

p.left {
    margin-top:0.5em;
    margin-bottom: 0.0em;
    text-indent:0.0em;
    text-align:left;
}

p.center {
    margin-top:0.0em;
    margin-bottom:0.25em;
    text-indent:0.0em;
    text-align:center;
}

h2 {
    margin-top:1em;
    font-size: 150%;
    text-indent: 0em;
    font-style: italic;
    text-align:center;
}

h3 {
    margin-top:1em;
    font-size: 125%;
    text-indent: 0em;
    text-align:center;
}

h4 {
    margin-top:1em;
    font-size: 125%;
    text-indent: 0em;
    text-align:left;
}

p.large {
    font-weight: bold;
    margin-top:1em;
    margin-bottom:1em;
    font-size: 200%;
    font-style: italic;
    text-indent: 0em;
    text-align:center;
}

p.medium {
    font-weight: bold;
    font-size: 150%;
    font-style: italic;
    margin-top:1.0em;
    margin-bottom:1.0em;
    text-indent: 0em;
    text-align:center;
}

p.small {
    font-weight: bold;
    margin-bottom:1em;
    font-size: 125%;
    text-indent: 0em;
    text-align:center;
}

p.block {
    font-family: courier, monospace;
    text-indent: 1em;
    text-align:left;
    margin:0em 0em 0em 1em;
}

p.blockfirst {
    font-family: courier, monospace;
    text-indent: 1em;
    text-align:left;
    margin:0.5em 0em 0em 1em;
}

p.blockcenter {
    font-family: courier, monospace;
    text-align:center;
    margin:0.5em 0em 0em 1em;
}

span.smallcap {
    font-size: 90%;
    font-weight: bold;
}

div.image {
    text-align:center;
    margin-bottom: 0.25em;
}

div.icon {
    text-align:center;
    margin-bottom: 1em;
}

div.caption {
    margin-bottom: 1em;
    text-align:center;
    font-style:italic;
}
'''
