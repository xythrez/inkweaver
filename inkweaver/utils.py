'''Common utilites used in InkWeaver.'''

import hashlib
import sys
import os
from pathlib import Path
from typing import Dict, IO, Type, Union


def sha256sum(data: Union[str, bytes]) -> str:
    '''Get SHA-256 checksum of string or bytestring.

    Args:
      data: String or bytestring to hash.

    Returns:
      Hexadecimal checksum of data.
    '''
    if isinstance(data, str):
        data = data.encode('UTF-8')
    msg = hashlib.sha256()
    msg.update(data)
    return msg.hexdigest()


def get_local_img_path(remote: str) -> str:
    '''Convert remote image URL to a local image URI.

    Args:
      remote: UTF-8 encoded URL to convert.

    Returns:
      Local image URI for within Ebook.
    '''
    suffix = Path(remote).suffix.split('?')[0]
    hash256 = sha256sum(remote)
    return f'images/{hash256}{suffix}'


class Singleton(type):
    '''Simple singleton metaclass.'''
    _instances: Dict[type, object] = {}

    def __call__(cls: Type, *args, **kwargs) -> object:
        if cls not in cls._instances:
            class_ = super(Singleton, cls).__call__(*args, **kwargs)
            cls._instances[cls] = class_
        return cls._instances[cls]


class TermHideCursor:
    '''Hide the terminal cursor'''

    # Terminal control sequences
    SEQ_HIDE_CURSOR: str = '\033[?25l'
    SEQ_SHOW_CURSOR: str = '\033[?25h'

    def __init__(self, hide: bool = True):
        self.hide = hide

    def __enter__(self):
        if self.hide:
            sys.stdout.write(self.SEQ_HIDE_CURSOR)
            sys.stdout.flush()
        return self

    def __exit__(self, exit_type, value, traceback):
        if self.hide:
            sys.stdout.write(self.SEQ_SHOW_CURSOR)
            sys.stdout.flush()
        return False


class InkWeaverError(Exception):
    '''Base error class for InkWeaver.'''


class DownloadError(InkWeaverError):
    '''Downloader error wrapper class.'''


class TemplateError(InkWeaverError):
    '''Template error wrapper class.'''


class EbookError(InkWeaverError):
    '''Generic epub error wrapper class.'''


class ProgressBar:
    '''Simple progress bar'''

    @property
    def term_width(self) -> int:
        '''Returns the terminal width.

        For noninteractive mode this returns 0.
        '''
        if self._noninteractive:
            return 0
        try:
            return os.get_terminal_size(1)[0]
        except OSError:
            return 0

    def __init__(self,
                 leftlen: int,
                 noninteractive: bool,
                 file: IO[str] = sys.stdout):
        self._noninteractive: bool = noninteractive
        self._progbar_msg: str = ''
        self._progbar_total: int = 0
        self._progbar_curr: int = 0
        self._progbar_leftlen: int = leftlen
        self._file = file

    def new(self, msg: str, total: int) -> None:
        '''Initializes a new progress bar.

        Only one progress bar can be used at a time. If the client is
        in non-interactive mode, only the message will be printed. If
        not enough space exists for the progress bar, only the minimal
        information will be shown.

        Args:
          msg: The message displayed on the bar.
          total: The total number of elements.
        '''
        width = self.term_width
        self._progbar_msg = msg
        self._progbar_total = total
        self._progbar_curr = 0
        progbar = None
        if width < self._progbar_leftlen:
            progbar = f'{msg}...\n'
        elif width < self._progbar_leftlen + 7:
            progbar = self._get_bar(width, progbar=False, percent=False)
        elif width < self._progbar_leftlen + 18:
            progbar = self._get_bar(width, progbar=False)
        else:
            progbar = self._get_bar(width)
        self._file.write(progbar)
        self._file.flush()

    def increment(self) -> bool:
        '''Increases the current value on the progress bar by one

        Returns:
          True if the bar is completely filled, false if otherwise.
        '''
        if (self.term_width < self._progbar_leftlen
                or self._progbar_curr >= self._progbar_total):
            return self._progbar_curr >= self._progbar_total
        self._progbar_curr += 1
        width = self.term_width
        progbar = None
        if width < self._progbar_leftlen + 7:
            progbar = self._get_bar(width, progbar=False, percent=False)
        elif width < self._progbar_leftlen + 18:
            progbar = self._get_bar(width, progbar=False)
        else:
            progbar = self._get_bar(width)
        self._file.write(f'\r{progbar}')
        if self._progbar_curr == self._progbar_total:
            self._file.write('\n')
        self._file.flush()
        return self._progbar_curr >= self._progbar_total

    def _get_bar(self,
                 width: int,
                 count: bool = True,
                 progbar: bool = True,
                 percent: bool = True) -> str:
        '''Generate a progress bar string.

        Args:
          width: Width of terminal.
          count: If true, shows the item counter.
          progbar: If true, shows the progress bar.
          percent: If true, shows completion percentage.

        Returns:
          The generated progress bar.
        '''
        progbar_str = self._progbar_msg
        pct = self._progbar_curr / self._progbar_total
        if count:
            countlen = len(str(self._progbar_total))
            progbar_str += f' ({str(self._progbar_curr).rjust(countlen)}'
            progbar_str += f'/{self._progbar_total})'
        if progbar or percent:
            progbar_str += ': '
            progbar_str = progbar_str.ljust(self._progbar_leftlen + 2)
        if progbar and self._progbar_leftlen + 17 < width:
            barlen = width - 10 - self._progbar_leftlen
            padding = 0
            if barlen > 100:
                padding = barlen - 100
                barlen = 100
            filled = int(barlen * pct)
            unfilled = barlen - filled
            progbar_str += ' ' * padding
            progbar_str += '['
            progbar_str += '#' * filled
            progbar_str += '-' * unfilled
            progbar_str += ']'
        if percent:
            pct_str = ''
            if pct * 100 > 0:
                pct_str = str(int(pct * 100)).rjust(3)
            else:
                pct_str = '  0'
            progbar_str += f' {pct_str}%'

        return progbar_str
