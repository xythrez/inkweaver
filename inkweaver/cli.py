'''InkWeaver command-line client components.'''

import sys
import os
import traceback
from argparse import ArgumentParser
from functools import reduce
from threading import Lock
from typing import Any, Dict, List

from .core import InkWeaver
from .reference import CLI_DEFAULTS, CONF_DEFAULTS
from .utils import (DownloadError, TemplateError, EbookError, TermHideCursor,
                    ProgressBar)


class CliHook:
    '''InkWeaverCli printer hook.'''

    # HookObject States
    STATE_UNKNOWN: int = 0
    STATE_DL_CHAPTER: int = 1
    STATE_PROC_CHAPTER: int = 2
    STATE_DL_IMAGE: int = 3
    STATE_SAVE_EBOOK: int = 4

    # Minimum terminal size to be counted as interactive
    MIN_INTERACTIVE: int = 33

    def __init__(self, noninteractive: bool):
        self._iolock: Lock = Lock()
        self._state: int = self.STATE_UNKNOWN
        self._noninteractive: bool = noninteractive
        self._progressbar: ProgressBar = ProgressBar(self.MIN_INTERACTIVE,
                                                     noninteractive)

    def on_chapter_dl(self, total: int) -> None:
        '''Hook called on chapter download success.

        Args:
          total: The total number of chapters.
        '''
        with self._iolock:
            if self._state != self.STATE_DL_CHAPTER:
                self._state = self.STATE_DL_CHAPTER
                self._progressbar.new('   Downloading chapters', total)
            self._progressbar.increment()

    def on_chapter_process(self, total: int) -> None:
        '''Hook called on chapter process.

        Args:
          total: The total number of chapters.
        '''
        if self._state != self.STATE_PROC_CHAPTER:
            self._state = self.STATE_PROC_CHAPTER
            self._progressbar.new('   Processing chapters', total)
        self._progressbar.increment()

    def on_image_dl(self, total: int) -> None:
        '''Hook called on image download success.

        Args:
          total: The total number of images.
        '''
        with self._iolock:
            if self._state != self.STATE_DL_IMAGE:
                self._state = self.STATE_DL_IMAGE
                self._progressbar.new('   Downloading images', total)
            self._progressbar.increment()

    def on_presave(self, output_path: str) -> None:
        '''Hook prior to saving ebook.

        Args:
          output_path: The output path for the ebook file.
        '''
        self._state = self.STATE_SAVE_EBOOK
        print(f':: Outputting ebook to "{output_path}"', flush=True)


class InkWeaverCli:
    '''InkWeaver command-line client object.'''

    # Operations
    OPS_UNKNOWN: int = 0
    OPS_LIST_SERIES: int = 1
    OPS_LIST_VOLUMES: int = 2
    OPS_DOWNLOAD_VOLUMES: int = 3

    @property
    def verbose(self) -> bool:
        '''Shorthand for getting InkWeaverCli verbosity'''
        if 'verbose' in self.cli_args:
            return self.cli_args['verbose']
        return CLI_DEFAULTS['verbose']

    @property
    def noninteractive(self) -> bool:
        '''Shorthand for getting InkWeaverCli interactiveness'''
        try:
            noterm = os.get_terminal_size(1)[0] <= 0
        except OSError:
            noterm = True
        if 'noninteractive' in self.cli_args:
            return self.cli_args['noninteractive'] or noterm
        return CLI_DEFAULTS['noninteractive'] or noterm

    def __init__(self, raw_args: List[str]) -> None:
        self.ops: int = self.OPS_UNKNOWN
        self.inkweaver = InkWeaver()
        self.raw_args: List[str] = raw_args
        self.cli_args: Dict[str, Any] = {}

    def get_version(self) -> str:
        '''Get the InkWeaver version string.

        Returns:
          String containing InkWeaver version and build number.
        '''
        major, minor = self.inkweaver.version
        return f'inkweaver v{major} build {minor}'

    def print_debug_info(self) -> None:
        '''Print client debug info.'''
        if not self.verbose:
            return
        out_str = ':: InkWeaver settings:\n'
        for key, value in self.inkweaver.arguments.items():
            out_str += f'   {key}: {value}\n'
        out_str += ':: InkWeaverCli settings:\n'
        for key, value in self.cli_args.items():
            out_str += f'   {key}: {value}\n'
        out_str += ':: Installed templates:'
        for series in self.inkweaver:
            out_str += f'\n   {series.series_id}: {series.template.__class__}'
        print(out_str)

    def run_print_series(self) -> None:
        '''Run print available series subroutine.'''
        if self.ops == self.OPS_UNKNOWN:
            self.ops = self._parse_args(self.raw_args[1:])
        series_str = ':: Available series:'
        for series in sorted(self.inkweaver, key=lambda x: x.series_id):
            series_str += (f'\n   {series.series_name} ({series.series_id})')
        print(series_str)

    def run_print_volumes(self, series_id: str) -> None:
        '''Run print available volumes in series subroutine.

        Args:
          series_id: ID of series to print volumes for.
        '''
        if self.ops == self.OPS_UNKNOWN:
            self.ops = self._parse_args(self.raw_args[1:])
        if series_id not in self.inkweaver:
            sys.exit(f'ERROR: "{series_id}" is not a valid series.'
                     '\nUse "--list" to see all available series.')
        series = self.inkweaver[series_id]
        if self.verbose and not series.initialized:
            print(':: Downloading Table of Contents for '
                  f'"{series.series_name}"')
        volume_str = f':: Available volumes for "{series.series_name}":'
        try:
            for volume in series:
                volume_str += (f'\n   {volume.volume_name} '
                               f'({len(volume)} chapter'
                               f'{"s" if len(volume) != 0 else ""})')
        finally:
            series.shutdown()
        print(volume_str)

    def run_download_volumes(self, series_id: str,
                             volume_ids: List[str]) -> None:
        '''Run download volumes subroutine.

        Args:
          series_id: ID of series to download volumes from.
          volume_ids: list of volumes to download, all if empty.
        '''
        if self.ops == self.OPS_UNKNOWN:
            self.ops = self._parse_args(self.raw_args[1:])
        if series_id not in self.inkweaver:
            sys.exit(f'ERROR: "{series_id}" is not a valid series.'
                     '\nUse "--list" to see all available series.')
        series = self.inkweaver[series_id]
        if self.verbose and not series.initialized:
            print(':: Downloading Table of Contents for '
                  f'"{series.series_name}"')
        try:
            missing = reduce(lambda x, y: y
                             if y not in series else x, volume_ids, None)
            if missing is not None:
                sys.exit(f'ERROR: "{missing}" is not a valid volume for '
                         f'"{series.series_name}".\n Use "--list {series_id}"'
                         ' to see all available volumes.')
            if self.verbose and not volume_ids:
                print(':: No volumes specified, downloading all volumes')
            for volume in [series[x] for x in volume_ids
                           ] if volume_ids else series.volumes:
                hook = CliHook(self.noninteractive)
                print(
                    f':: Creating ebook for "{series.series_name} - '
                    f'{volume.volume_name}"',
                    flush=True)
                volume.save(hook.on_chapter_dl, hook.on_chapter_process,
                            hook.on_image_dl, hook.on_presave)
                print('   Ebook saved successfully', flush=True)
        finally:
            series.shutdown()

    def run(self) -> None:
        '''Run InkWeaver command-line client.'''
        self.ops = self._parse_args(self.raw_args)
        try:
            with TermHideCursor(not self.noninteractive):
                self.print_debug_info()
                if self.ops == self.OPS_LIST_SERIES:
                    self.run_print_series()
                elif self.ops == self.OPS_LIST_VOLUMES:
                    self.run_print_volumes(self.cli_args['series'])
                elif self.ops == self.OPS_DOWNLOAD_VOLUMES:
                    self.run_download_volumes(self.cli_args['series'],
                                              self.cli_args['volumes'])
        except (DownloadError, TemplateError, EbookError) as err:
            print(
                '\nERROR: InkWeaver has encountered a '
                f'{err.__class__.__qualname__}!',
                file=sys.stderr)
            if not self.verbose:
                print('Use "--verbose" to see full stacktrace.')
            else:
                traceback.print_exception(type(err), err, err.__traceback__)
            sys.exit('Exiting due to errors.')
        except KeyboardInterrupt:
            sys.exit('\nERROR: Interrupted by user.')

    def _parse_args(self, args: List[str]) -> int:
        '''Parse command-line arguments using argparse.

        Args:
          args: Command-line arguments.

        Returns:
          Operation to perform.

        Raises:
          NameError: If key is invalid.
        '''
        parser = self._create_parser(os.path.basename(args[0]))
        options = vars(parser.parse_args(args[1:]))

        for opt_k, opt_v in options.items():
            if opt_k in CLI_DEFAULTS:
                self.cli_args[opt_k] = opt_v
            elif opt_k in CONF_DEFAULTS:
                if CONF_DEFAULTS[opt_k] != opt_v:
                    self.inkweaver.set_argument(opt_k, opt_v)
            else:
                raise NameError(f'{opt_k} is not a valid argument!')

        if self.cli_args['list'] and self.cli_args['series']:
            return self.OPS_LIST_VOLUMES
        if self.cli_args['list']:
            return self.OPS_LIST_SERIES
        if self.cli_args['series']:
            return self.OPS_DOWNLOAD_VOLUMES
        parser.print_usage(sys.stderr)
        sys.exit(os.EX_NOINPUT)

    def _create_parser(self, prog: str) -> ArgumentParser:
        '''Create an ArgumentParser object.

        Returns:
          Generated ArgumentParser.
        '''
        parser = ArgumentParser(prog=prog,
                                description='website to epub converter.')
        dl_opt = parser.add_argument_group('download options')
        out_opt = parser.add_argument_group('output options')
        dl_opt.add_argument('-t',
                            '--timeout',
                            action='store',
                            metavar='seconds',
                            type=int,
                            default=CONF_DEFAULTS['timeout'],
                            help=('maximum number of seconds allowed to '
                                  'establish a connection. '
                                  f'(Default: {CONF_DEFAULTS["timeout"]})'))
        dl_opt.add_argument('-I',
                            '--interval',
                            action='store',
                            metavar='seconds',
                            type=int,
                            default=CONF_DEFAULTS['interval'],
                            help=('time to wait after downloading a resource. '
                                  f'(Default: {CONF_DEFAULTS["interval"]})'))
        dl_opt.add_argument(
            '-r',
            '--max-retries',
            action='store',
            metavar='retries',
            type=int,
            default=CONF_DEFAULTS['max_retries'],
            help=('maximum number of retries a connection is '
                  'allowed to fail. '
                  f'(Default: {CONF_DEFAULTS["max_retries"]})'))
        dl_opt.add_argument('-T',
                            '--threads',
                            action='store',
                            metavar='threads',
                            type=int,
                            default=CONF_DEFAULTS['threads'],
                            help=('number of simultaneous download threads. '
                                  f'(Default: {CONF_DEFAULTS["threads"]})'))
        dl_opt.add_argument('-u',
                            '--user-agent',
                            action='store',
                            metavar='agent',
                            default=CONF_DEFAULTS['user_agent'],
                            help='user-agent used by the downloader.')
        out_opt.add_argument('-o',
                             '--output-format',
                             action='store',
                             metavar='format',
                             default='%SERIES% - %VOLUME%.epub',
                             help=('format for the output name, variables '
                                   '"%%SERIES%%" and "%%VOLUME%%" are '
                                   'automatically replaced.'))
        out_opt.add_argument('-N',
                             '--noninteractive',
                             action='store_true',
                             help=('disable pretty print and confirmations. '
                                   'This flag is automatically enabled when '
                                   'the output is being piped.'))
        out_opt.add_argument('-V',
                             '--verbose',
                             action='store_true',
                             help=('print additional information to the '
                                   'terminal.'))
        parser.add_argument('-l',
                            '--list',
                            action='store_true',
                            help=('list available volumes for a given series. '
                                  'If no series is given, available series '
                                  'are listed instead.'))
        parser.add_argument('-v',
                            '--version',
                            action='version',
                            version=self.get_version(),
                            help='show program version and exit.')
        parser.add_argument('series',
                            action='store',
                            nargs='?',
                            help='series to download from.')
        parser.add_argument('volumes',
                            action='store',
                            nargs='*',
                            help='volumes selected for download.')
        return parser
