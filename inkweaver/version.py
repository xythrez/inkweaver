'''Version file for InkWeaver.

All occurances of '%DEV_VERSION%' are automatically replaced by the buildscript
with the date of build.
'''

from typing import Union, Tuple

INKWEAVER_MAJOR_VERSION: int = 4
INKWEAVER_MINOR_VERSION: Union[int, str] = '%DEV_VERSION%'  # Updated via make.

INKWEAVER_VERSION: Tuple[int, Union[int, str]] = (INKWEAVER_MAJOR_VERSION,
                                                  INKWEAVER_MINOR_VERSION)
__version__: str = '.'.join(map(str, INKWEAVER_VERSION))
