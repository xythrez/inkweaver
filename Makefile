BIN ?= inkweaver
SRCDIR ?= inkweaver
BLDDIR ?= build
PYTHON ?= /usr/bin/env python3
REQLIST ?= requirements.txt

VER_FILE := version.py
EXTDIR ?= ext
TEMPLATES_DIR := inkweaver/templates
EXT_TEMPLATES := $(wildcard $(EXTDIR)/*.py $(EXTDIR)/**/*.py)
TIMESTAMP := $(shell date -u "+%Y%m%d")

.DEFAULT_GOAL := $(BLDDIR)/$(BIN)
.PHONY: init build clean purge

venv:
	$(PYTHON) -m venv venv
	venv/bin/pip install -r $(REQLIST)

$(BLDDIR)/$(BIN): requirements.txt $(SRCDIR)/*.py $(SRCDIR)/**/*.py $(EXT_TEMPLATES)
	rm -rf "$(BLDDIR)/src"
	mkdir -p "$(BLDDIR)/src"
	find "$(SRCDIR)" -type f -name "*.py" -exec install -Dm 644 "{}" "$(BLDDIR)/src/{}" \;
	-find "$(EXTDIR)" -type f -name "*.py" -exec install -m 644 "{}" "$(BLDDIR)/src/$(TEMPLATES_DIR)" \;
	install -Dm 644 "$(SRCDIR)/__main__.py" "$(BLDDIR)/src/__main__.py"
	sed -i "s/'%DEV_VERSION%'/$(TIMESTAMP)/g" "$(BLDDIR)/src/$(BIN)/$(VER_FILE)"
	sed -e "/^lxml/d" -e "/^pycurl/d" "$(REQLIST)" > $(BLDDIR)/requirements.txt
	$(PYTHON) -m pip install -Ir "$(BLDDIR)/requirements.txt" --target "$(BLDDIR)/src" --no-deps
	find "$(BLDDIR)/src" -maxdepth 1 -name "*.egg-info" -exec rm -rf "{}" +
	find "$(BLDDIR)/src" -maxdepth 1 -name "*.dist-info" -exec rm -rf "{}" +
	$(PYTHON) -m compileall "$(BLDDIR)/src"
	find "$(BLDDIR)/src" -exec touch -t 198001010000 "{}" \;
	$(PYTHON) -m zipapp -p "$(PYTHON)" "$(BLDDIR)/src" -o "$(BLDDIR)/$(BIN)"

init: venv

build: $(BLDDIR)/$(BIN)

clean:
	rm -rf "$(BLDDIR)" **/*.epub *.epub
	find -name "__pycache__" -exec rm -rf "{}" +
	find -name ".mypy_cache" -exec rm -rf "{}" +

purge: clean
	rm -rf venv/
