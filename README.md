InkWeaver - Website to Epub Converter
=====================================

InkWeaver is a website to Epub converter that operates using templates. This
project contains the core InkWeaver API along with a simple Linux Cli client.

Usage
-----

To use the command-line client:

```bash
$ ./inkweaver [--flags...] [series] [volumes...]
```

See `--help` for more details.


To use the InkWeaver API:

```python
from inkweaver import InkWeaver

# Save a specific volume from a series
inkweaver = InkWeaver()
inkweaver['series_name']['volume_name'].save()
```

Requirements
------------
- Python3 >= 3.6.0
- lxml
- pycurl
- pip

Build
-----

To build InkWeaver, run `make` in the repo root directory. The generated binary
will be located in `build/`.

Custom Templates
----------------

Custom templates included in `ext/` will also be included in the inkweaver
binary. This allows the use of private template repos.

License
-------

This project is licensed under GPLv3 or newer. See `LICENSE` for more details.
